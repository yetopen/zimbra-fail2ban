# Zimbra rules for fail2ban

Relying on Zimbra's DoSFilter is generally better, or necessary in multi server environment. For setting up DoSFilter see
[Mark Stone excellent tutorial](https://www.missioncriticalemail.com/2018/10/19/using-zimbras-dosfilter-and-failed-login-lockout-policy-together/).

If you prefer using fail2ban just install it and copy `filter.d/*` to your `/etc/fail2ban/filter.d`. 

Then include the relevant part of [jail.local.zimbra](jail.local.zimbra) into your `/etc/fail2ban/jail.local`.

Please check and understand the various parameters. We use rather aggressive values for `bantime` and `maxretry`, they
could be too tight for your environment.

Also please note we often use `iptables-allports` as default action. Change if you believe it's too wide.

## Fail2ban for Ubuntu 12.04

Given that it's out of LTS it remain widely used. The repo version is rather old, we suggest installing Neuro's repo version with:

```bash
wget -O- http://neuro.debian.net/lists/precise.de-m.full | tee /etc/apt/sources.list.d/neurodebian.sources.list
apt-key adv --recv-keys --keyserver hkp://pgp.mit.edu:80 0xA5D32F012649A5A9
apt-get update
apt-get install fail2ban
```
